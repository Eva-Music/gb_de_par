from pprint import pprint

import requests
from bs4 import BeautifulSoup as bs
import time
import pandas as pd
from pymongo import MongoClient
import re

MONGO_URI = "localhost:27017"
MONGO_DB = "jobs"

url_hh = "https://hh.ru/search/vacancy?"
url_sj = "https://www.superjob.ru/vacancy/search/?"

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/87.0.4280.141 Safari/537.36"
}


def get_hh_params(job, page_num):
    return {
        "page": page_num,
        "area": "1",
        "st": "searchVacancy",
        "text": job.replace(" ", "+"),
    }


def get_sj_params(job, page_num):
    return {
        "keywords": job,
        "page": page_num,
    }


def get_request(url, params):
    return requests.get(url, headers=headers, params=params)


def get_hh_jobs_list(job, page_num):
    jobs_info = []
    page = 0
    for i in range(page_num):
        params = get_hh_params(job, page)
        response = get_request(url_hh, params)
        time.sleep(1)
        soup = bs(response.text, "html.parser")

        vacancy_box = soup.findAll(attrs={"class": "vacancy-serp-item"})
        if len(vacancy_box) == 0: break
        for item in vacancy_box:
            info = {}
            name = item.find(attrs={"data-qa": "vacancy-serp__vacancy-title"})
            info['job_name'] = name.text
            info['job_link'] = name.attrs["href"]

            compensation = item.find(attrs={"data-qa": "vacancy-serp__vacancy-compensation"})
            if compensation is not None:
                text = compensation.text
                if text.find("до") != -1 and text.find("от") != -1:
                    text.replace("до", "-")

                temp = re.sub('[^0-9–]', '', text)
                if temp.find("–") != -1:
                    salary = temp.split("–")
                    info['salary_min'] = int(salary[0])
                    info['salary_max'] = int(salary[1])
                else:
                    info['salary_max'] = int(temp)
                    info['salary_min'] = int(temp)
            else:
                info['salary_max'] = 0
                info['salary_min'] = 0

            info['job_aggregator'] = url_hh.split("/")[2]
            jobs_info.append(info)
        page += 1
    return jobs_info


def get_sj_jobs_list(job, page_num):
    jobs_info = []
    page = 1
    for i in range(page_num):
        params = get_sj_params(job, page)
        response = get_request(url_sj, params)
        time.sleep(1)
        soup = bs(response.text, "html.parser")

        vacancy_box = soup.findAll(attrs={"class": "f-test-vacancy-item"})
        if len(vacancy_box) == 0: break
        for item in vacancy_box:
            info = {}
            name = item.select("a[class*='f-test-link-']")[0]

            info['job_name'] = name.text
            info['job_link'] = "https://russia.superjob.ru/" + name.attrs["href"]

            compensation = item.find(attrs={"class": "f-test-text-company-item-salary"})
            text = compensation.text
            if text.find("По") != -1:
                info['salary_max'] = 0
                info['salary_min'] = 0
            else:
                temp = re.sub('[^0-9—]', '', text)
                if temp.find("—") != -1:
                    salary = temp.split("—")
                    info['salary_min'] = int(salary[0])
                    info['salary_max'] = int(salary[1])
                else:
                    info['salary_max'] = int(temp)
                    info['salary_min'] = int(temp)

            info['job_aggregator'] = url_sj.split("/")[2]
            jobs_info.append(info)
        page += 1
    return jobs_info


def pandas_table(arr):
    pd.set_option('display.max_columns', None)
    pd.set_option("max_rows", None)
    pd.set_option('display.max_colwidth', 0)
    pd.set_option('display.width', None)
    pd.set_option('display.colheader_justify', 'left')
    df = pd.DataFrame.from_records(arr)
    print(df)


def show_grt_salary_in_db(db: MongoClient, value):
    result = db.find({
        "salary_min": {
            "$gte": value
        }
    })
    for item in result:
        pprint(item)


def show_range_salary_in_db(db: MongoClient, fromm, to):
    result = db.find({
        "$and": [
            {"salary_min": {"$gt": fromm}},
            {"salary_max": {"$lt": to}}
        ]
    })
    for item in result:
        pprint(item)


vacancy = input("Введите название вакансии: ")
print()
pages = int(input("Сколько страниц сайта надо проанализировать:"))
print()

hh_jobs = get_hh_jobs_list(vacancy, pages)
pandas_table(hh_jobs)

print()

sj_jobs = get_sj_jobs_list(vacancy, pages)
pandas_table(sj_jobs)

with MongoClient(MONGO_URI) as client:
    main_db = client[MONGO_DB]
    hh_jobs_db = main_db["hh_jobs"]
    sj_jobs_db = main_db["sj_jobs"]

    # hh_jobs_db.insert_many(hh_jobs)
    # sj_jobs_db.insert_many(sj_jobs)

    hh_jobs_db.update_many(hh_jobs, upsert=True)
    sj_jobs_db.update_many(sj_jobs, upsert=True)

    bigger_salary = int(input("Вывести вакансии с зп больше:"))
    show_grt_salary_in_db(hh_jobs_db, bigger_salary)
    show_grt_salary_in_db(sj_jobs_db, bigger_salary)
    print()

    from_salary = int(input("Вывести вакансии с зп в диапазоне от:"))
    print()
    to_salary = int(input("до:"))
    show_range_salary_in_db(hh_jobs_db, from_salary, to_salary)
    show_range_salary_in_db(sj_jobs_db, from_salary, to_salary)
