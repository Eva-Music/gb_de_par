# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import scrapy
from scrapy.pipelines.images import ImagesPipeline
from scrapy.utils.python import to_bytes
import hashlib
from pymongo import MongoClient


class LMPhotosPipeline(ImagesPipeline):

    def get_media_requests(self, item, info):
        if item["photos"]:
            for img in item["photos"]:
                try:
                    yield scrapy.Request(img)
                except Exception as e:
                    print(e)

    def item_completed(self, results, item, info):
        if results:
            item["photos"] = [itm[1] for itm in results if itm[0]]
        return item

    def get_images(self, response, request, info, *, item=None):
        # Единственная проблема, что вот тут почему-то появляется ошибка
        # ValueError: too many values to unpack (expected 3) в image_downloaded
        # и пишет, что он не можнт обработать столько аргументов: странно,
        # оч долго пыталась исправить, не получается
        yield self.file_path(request, response=response, info=info, item=item)

    def file_path(self, request, response=None, info=None, *, item=None):
        image_guid = hashlib.sha1(to_bytes(request.url)).hexdigest()
        return f'images/{item["name"].split(" ")[0]}/{image_guid}.jpg'


class Lesson7Pipeline:
    def __init__(self):
        self.client = MongoClient("localhost:27017")
        self.db = self.client["LM"]

    def process_item(self, item, spider: scrapy):
        with self.client:
            if not self.db[spider.name].find_one({'name': item['name']}):
                self.db[spider.name].insert_one(item)
            else:
                self.db[spider.name].update_one({"name": item["name"]}, {"$set": item}, upsert=True)
        return item

    def close_spider(self, spider):
        self.client.close()
