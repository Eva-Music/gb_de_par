from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from urllib.parse import quote_plus

from lesson_7 import settings
from lesson_7.spiders.lm import LmSpider

if __name__ == "__main__":
    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    search = quote_plus(input())
    process = CrawlerProcess(settings=crawler_settings)
    process.crawl(LmSpider, search=search)

    process.start()
