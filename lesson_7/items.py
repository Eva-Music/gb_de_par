# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from itemloaders.processors import MapCompose, TakeFirst, Compose
from w3lib.html import remove_tags


def make_dic(self):
    param_list = self.replace(" ", "").split("\n")
    return {param_list[1]: param_list[3]}


def make_int(self):
    return int(self[0].replace(" ", ""))


class Lesson7Item(scrapy.Item):
    _id = scrapy.Field()
    name = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=TakeFirst(),
    )
    photos = scrapy.Field()
    params = scrapy.Field(
            input_processor=MapCompose(remove_tags),
            output_processor=MapCompose(make_dic)
    )
    url = scrapy.Field(
        output_processor=TakeFirst(),
    )
    price = scrapy.Field(
        input_processor=MapCompose(remove_tags),
        output_processor=Compose(make_int),
    )
