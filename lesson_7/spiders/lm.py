import scrapy
from scrapy.http import HtmlResponse
from scrapy.loader import ItemLoader
from lesson_7.items import Lesson7Item


class LmSpider(scrapy.Spider):
    name = 'lm'
    allowed_domains = ['leroymerlin.ru']

    def __init__(self, search):
        super(LmSpider, self).__init__()
        self.start_urls = [f'https://leroymerlin.ru/search/?q={search}']

    def parse(self, response: HtmlResponse):
        links = response.xpath("//*[@data-qa-product]/a")
        for link in links:
            yield response.follow(link, callback=self.parse_item)

        next_page = response.xpath("//*[contains(@aria-label, 'Следующая страница')]")
        if next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_item(self, response: HtmlResponse):
        loader = ItemLoader(item=Lesson7Item(), response=response)
        loader.add_xpath("name", "//h1/text()")
        loader.add_xpath("photos", "//img[@slot = 'thumbs']/@src")
        loader.add_xpath("params", "//div[@class = 'def-list__group']")
        loader.add_value("url", response.url)
        loader.add_xpath("price", "//*[@class = 'primary-price']//*[@slot = 'price']")
        yield loader.load_item()