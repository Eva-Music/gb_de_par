import scrapy
from scrapy.http import HtmlResponse
from parser_6.items import Parser6Item
import re


class LabirintSpider(scrapy.Spider):
    name = 'labirint'
    allowed_domains = ['labirint.ru']
    start_urls = ['http://labirint.ru/books/']

    def parse(self, response: HtmlResponse):
        book_urls = response.xpath("//*[@class = 'content-block ']"
                                   "//*[@class = 'product-cover']"
                                   "//*[@class = 'cover']/@href").getall()
        for url in book_urls:
            yield response.follow(url, callback=self.process_item)

        next_page = response.xpath("//*[@class = 'pagination-next']"
                                   "//*[@class = 'pagination-next__text']"
                                   "/@href").get()
        if next_page:
            yield response.follow(next_page, callback=self.parse)
        pass

    def process_item(self, response: HtmlResponse):
        name = response.xpath("//*[@id = 'product-title']/h1/text()").get()
        author = response.xpath("//*[@class = 'product-description']"
                                "/*[@class = 'authors'][1]/a/text()").getall()
        price = response.xpath("//*[@class = 'buying-priceold-val-number']/text()").get()
        discount_price = response.xpath("//*[@class = 'buying-pricenew-val-number']/text()").get()
        rate = response.xpath("//*[@id = 'rate']/text()").get()
        item = Parser6Item()
        item["url"] = response.url
        item["name"] = name
        item["author"] = author
        item["price"] = int(re.search(r'[0-9]+', price).group())
        item["discount_price"] = int(re.search(r'[0-9]+', discount_price).group())
        item["rate"] = int(re.search(r'[0-9]+', rate).group())
        yield item
