import scrapy
from scrapy.http import HtmlResponse
from parser_6.items import Parser6Item
import re


class Book24Spider(scrapy.Spider):
    name = 'book24'
    allowed_domains = ['book24.ru']
    start_urls = ['https://book24.ru/catalog-sale/']

    def parse(self, response: HtmlResponse):
        book_urls = response.xpath("//*[@class = 'product-list__item']"
                                   "//*[@class = 'product-card__image-link smartLink']"
                                   "/@href").getall()
        for url in book_urls:
            yield response.follow(url, callback=self.process_item)

        next_page = response.xpath("//a[@class = 'pagination__item _link _button _next smartLink']"
                                   "/@href").get()
        if next_page:
            yield response.follow(next_page, callback=self.parse)
        pass

    def process_item(self, response: HtmlResponse):
        name = response.xpath("//*[@class = 'item-detail__title']/text()").get()
        author = response.xpath("//*[@class = 'item-tab']"
                                "//a[@itemprop = 'author']/text()").getall()
        price = response.xpath("//*[@class = 'item-actions__price-old']"
                               "/text()").get()
        discount_price = response.xpath("//*[@class = 'item-actions__price']"
                                        "//*[@itemprop = 'price']"
                                        "/text()").get()
        rate = response.xpath("//*[contains(@class, 'rating__rate-value')]/text()").get()
        item = Parser6Item()
        item["url"] = response.url
        item["name"] = name
        item["author"] = author
        item["price"] = int(re.search(r'[0-9]+', price).group())
        item["discount_price"] = int(re.search(r'[0-9]+', discount_price).group())
        item["rate"] = int(re.search(r'[0-9]+', rate).group())
        yield item
