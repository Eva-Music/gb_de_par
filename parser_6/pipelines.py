# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
from pymongo import MongoClient
import scrapy
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


class Parser6Pipeline:
    def __init__(self):
        self.client = MongoClient("localhost:27017")
        self.db = self.client["bookstore"]

    def process_item(self, item, spider: scrapy):
        with self.client:
            if not self.db[spider.name].find_one({'name': item['name'], 'author': item['author']}):
                self.db[spider.name].insert_one(item)
            else:
                self.db[spider.name].update_one({"name": item["name"]}, {"$set": item}, upsert=True)
        return item
