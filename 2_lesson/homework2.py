import requests
from bs4 import BeautifulSoup as bs
import time
import pandas as pd

url_hh = "https://hh.ru/search/vacancy?"
url_sj = "https://www.superjob.ru/vacancy/search/?"

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/87.0.4280.141 Safari/537.36"
}


def get_hh_params(job, page_num):
    return {
        "page": page_num,
        "area": "1",
        "st": "searchVacancy",
        "text": job.replace(" ", "+"),
    }


def get_sj_params(job, page_num):
    return {
        "keywords": job,
        "page": page_num,
    }


def get_request(url, params):
    return requests.get(url, headers=headers, params=params)


def get_hh_jobs_list(job, page_num):
    jobs_info = []
    page = 0
    for i in range(page_num):
        params = get_hh_params(job, page)
        response = get_request(url_hh, params)
        time.sleep(1)
        soup = bs(response.text, "html.parser")

        vacancy_box = soup.findAll(attrs={"class": "vacancy-serp-item"})
        if len(vacancy_box) == 0: break
        for item in vacancy_box:
            info = {}
            name = item.find(attrs={"data-qa": "vacancy-serp__vacancy-title"})
            info['job_name'] = name.text
            info['job_link'] = name.attrs["href"]

            compensation = item.find(attrs={"data-qa": "vacancy-serp__vacancy-compensation"})
            if compensation is not None:
                info['salary'] = compensation.text
            else:
                info['salary'] = "Не указано"

            info['job_aggregator'] = url_hh.split("/")[2]
            jobs_info.append(info)
        page += 1
    return jobs_info


def get_sj_jobs_list(job, page_num):
    jobs_info = []
    page = 1
    for i in range(page_num):
        params = get_sj_params(job, page)
        response = get_request(url_sj, params)
        time.sleep(1)
        soup = bs(response.text, "html.parser")

        vacancy_box = soup.findAll(attrs={"class": "f-test-vacancy-item"})
        if len(vacancy_box) == 0: break
        for item in vacancy_box:
            info = {}
            name = item.select("a[class*='f-test-link-']")[0]

            info['job_name'] = name.text
            info['job_link'] = "https://russia.superjob.ru/" + name.attrs["href"]

            compensation = item.find(attrs={"class": "f-test-text-company-item-salary"})
            info['salary'] = compensation.text

            info['job_aggregator'] = url_sj.split("/")[2]
            jobs_info.append(info)
        page += 1
    return jobs_info


def pandas_table(arr):
    pd.set_option('display.max_columns', None)
    pd.set_option("max_rows", None)
    pd.set_option('display.max_colwidth', 0)
    pd.set_option('display.width', None)
    pd.set_option('display.colheader_justify', 'left')
    df = pd.DataFrame.from_records(arr)
    print(df)


vacancy = input("Введите название вакансии: ")
print()
pages = int(input("Сколько страниц сайта надо проанализировать:"))
print()

hh_jobs = get_hh_jobs_list(vacancy, pages)
pandas_table(hh_jobs)

print()

sj_jobs = get_sj_jobs_list(vacancy, pages)
pandas_table(sj_jobs)