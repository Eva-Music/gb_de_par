from pprint import pprint

import requests
import json

url = "https://api.github.com/users/{}/repos"
user_name = "samdutton"  # google developer
path = "repos.json"

r = requests.get(url.format(user_name))

if r.status_code == 200:
    with open(path, "w") as f:
        json.dump(r.json(), f, indent=2)

with open(path, "r") as f:
    data = f.read()
    json_data = json.loads(data)
    print(json.dumps(json_data, indent=4, sort_keys=True))
