import requests
import json

api_key = ""
url = "http://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}&units=metric"

city_name = input("Write city name: ")
print()
print(city_name)

r = requests.post(url.format(city_name, api_key))

if r.status_code == 200:
    print(r.json()["weather"][0]["description"])
    print(json.dumps(r.json()["main"], indent=2))
    # print(r.json()["main"])
else:
    print("Invalid city name")



