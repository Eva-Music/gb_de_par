from pprint import pprint

from pymongo import MongoClient

client = MongoClient("localhost:27017")
db = "Insta"
names = ['grangeprodto', 'cookingfreak696']
with client:
    insta_db = client[db]
    for name in names:
        user_data_db = insta_db[name]

        follows = user_data_db.distinct('follows')

        for item in follows:
            pprint(item)

        followers = user_data_db.distinct('followers')

        for item in followers:
            pprint(item)