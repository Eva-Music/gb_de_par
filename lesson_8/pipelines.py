# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

from pymongo import MongoClient


class Lesson8Pipeline:
    def __init__(self):
        self.client = MongoClient("localhost:27017")
        self.db = self.client["Insta"]

    def process_item(self, item):
        with self.client:
            if not self.db[item['username']].find_one({'user_id': item['user_id']}):
                self.db[item['username']].insert_one(item)
            else:
                if item["follows"] is None:
                    if not self.db[item['username']].find_one(
                            {'followers.follower_id': item["followers"][0]["follower_id"]}):
                        self.db[item['username']].update_one({"user_id": item["user_id"]},
                                                             {"$addToSet":
                                                                  {"followers": item["followers"][0]}
                                                              })
                else:
                    if not self.db[item['username']].find_one(
                            {'follows.follower_id': item["follows"][0]["follower_id"]}):
                        self.db[item['username']].update_one({"user_id": item["user_id"]},
                                                             {"$addToSet":
                                                                  {"follows": item["follows"][0]}
                                                              })
        return item

    def close_spider(self):
        self.client.close()
