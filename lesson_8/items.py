# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class FollowerItem(scrapy.Item):
    follower_id = scrapy.Field()
    follower_name = scrapy.Field()
    follower_photo = scrapy.Field()


class InstaItem(scrapy.Item):
    # define the fields for your item here like:
    _id = scrapy.Field()
    user_id = scrapy.Field()
    username = scrapy.Field()
    followers: list = scrapy.Field()
    follows: list = scrapy.Field()
