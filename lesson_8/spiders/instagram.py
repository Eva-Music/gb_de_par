import scrapy
from scrapy.http import HtmlResponse
import re
import json
from urllib.parse import quote
from copy import deepcopy
from lesson_8.items import InstaItem
from lesson_8.items import FollowerItem


class InstaSpider(scrapy.Spider):
    name = 'instagram'
    allowed_domains = ['instagram.com']
    start_urls = ['https://www.instagram.com/']
    login_url = "https://www.instagram.com/accounts/login/ajax/"
    username = "sharon94_pars"
    enc_password = "#PWD_INSTAGRAM_BROWSER:10:1620669461:AYhQAB02B9J59x9xUlzaXEu2I0Vq4tpVBoCE3Njbs8DLcGsyLDW9vx27JEQ8" \
                   "7m1enNBczF0gsudW4lyRcKCiyppY3QEBED6LYNizgLrzu0BdysBxPchmD1im+OlCUygGhAKAY8WpefWEkswjiWA="
    user_to_parse_url_template = "/%s"
    followers_hash = "5aefa9893005572d237da5068082d8d5"
    follows_hash = "3dec7e2c57367ef3da3d987d89f9dbc8"
    graphql_url = "https://www.instagram.com/graphql/query/?"

    def __init__(self, users_list):
        super(InstaSpider, self).__init__()
        self.users_list = users_list

    def parse(self, response: HtmlResponse):
        csrf_token = self.fetch_csrf_token(response.text)
        yield scrapy.FormRequest(
            self.login_url,
            method="POST",
            callback=self.user_login,
            formdata={
                "username": self.username,
                "enc_password": self.enc_password
            },
            headers={
                'x-csrftoken': csrf_token
            }
        )

    def user_login(self, response: HtmlResponse):
        data = response.json()
        for user in self.users_list:
            if data['authenticated']:
                yield response.follow(
                    self.user_to_parse_url_template % user,
                    callback=self.user_data_parse,
                    cb_kwargs={
                        "username": user,
                    }
                )

    def make_str_variables(self, variables):
        str_variables = quote(
            str(variables).replace(" ", "").replace("'", '"')
        )
        return str_variables

    def user_data_parse(self, response: HtmlResponse, username):
        user_id = self.fetch_user_id(response.text, username)
        variables = {
            'first': 12,
            'id': user_id
        }
        str_variables = self.make_str_variables(variables)
        followers_url = f"{self.graphql_url}query_hash={self.followers_hash}&variables={str_variables}"
        yield response.follow(
            followers_url,
            callback=self.user_followers_parse,
            cb_kwargs={
                "username": username,
                "user_id": user_id,
                "variables": deepcopy(variables),
            }
        )

        follows_url = f"{self.graphql_url}query_hash={self.follows_hash}&variables={str_variables}"
        yield response.follow(
            follows_url,
            callback=self.user_follows_parse,
            cb_kwargs={
                "username": username,
                "user_id": user_id,
                "variables": deepcopy(variables),
            }
        )

    def user_followers_parse(self, response: HtmlResponse, username, user_id, variables):
        data = response.json()
        info = data["data"]["user"]['edge_followed_by']
        followers = info['edges']
        for follower in followers:
            item = InstaItem()
            followers_item = FollowerItem()

            item['user_id'] = user_id
            item['username'] = username
            node = follower['node']

            followers_item['follower_id'] = node['id']
            followers_item['follower_name'] = node['username']
            followers_item['follower_photo'] = node['profile_pic_url']
            l = [followers_item]
            item['followers'] = l
            yield item

        page_info = info['page_info']
        if page_info['has_next_page']:
            variables['after'] = page_info['end_cursor']
            str_variables = self.make_str_variables(variables)
            url = f"{self.graphql_url}query_hash={self.followers_hash}&variables={str_variables}"
            yield response.follow(
                url,
                callback=self.user_followers_parse,
                cb_kwargs={
                    "username": username,
                    "user_id": user_id,
                    "variables": deepcopy(variables),
                }
            )

    def user_follows_parse(self, response: HtmlResponse, username, user_id, variables):
        data = response.json()
        info = data["data"]["user"]['edge_follow']
        follows = info['edges']
        for follow in follows:
            item = InstaItem()
            follow_item = FollowerItem()

            item['user_id'] = user_id
            item['username'] = username
            node = follow['node']

            follow_item['follower_id'] = node['id']
            follow_item['follower_name'] = node['username']
            follow_item['follower_photo'] = node['profile_pic_url']
            l = [follow_item]
            item['follows'] = l
            yield item

        page_info = info['page_info']
        if page_info['has_next_page']:
            variables['after'] = page_info['end_cursor']
            str_variables = self.make_str_variables(variables)
            url = f"{self.graphql_url}query_hash={self.follows_hash}&variables={str_variables}"
            yield response.follow(
                url,
                callback=self.user_follows_parse,
                cb_kwargs={
                    "username": username,
                    "user_id": user_id,
                    "variables": deepcopy(variables),
                }
            )

    def fetch_csrf_token(self, text):
        matched = re.search('\"csrf_token\":\"\\w+\"', text).group()
        return matched.split(':').pop().replace(r'"', '')

    def fetch_user_id(self, text, username):
        matched = re.search(
            '{\"id\":\"\\d+\",\"username\":\"%s\"}' % username, text
        ).group()
        return json.loads(matched).get('id')
