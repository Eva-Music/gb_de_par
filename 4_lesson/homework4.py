from pymongo import MongoClient
import requests
from lxml import html
from datetime import datetime

MONGO_URI = "localhost:27017"
MONGO_DB = "news"

url_mail_news = "https://news.mail.ru/"
url_lenta = "https://lenta.ru"
url_ya_news = "https://yandex.ru/news"

headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_1) AppleWebKit/537.36 (KHTML, like Gecko) "
                  "Chrome/87.0.4280.141 Safari/537.36"
}


def add_unique_job(db, job_info):
    if not db.find_one({'_id': job_info['_id']}):
        db.insert_one(job_info)


## mail
r = requests.get(url_mail_news, headers=headers)
main_xpath = "//div[contains(@class, 'daynews__item')]"
dom = html.fromstring(r.text)
items = dom.xpath(main_xpath)

info_list_mail = []

for item in items:
    info = {}
    xpath_item_name = "//span[contains(@class, 'photo__title')]/text()"
    xpath_item_href = "//a[contains(@class, 'photo_full')]/@href"
    xpath_data = "//div[contains(@class, 'breadcrumbs_article')]//span[contains(@class, 'js-ago')]/@datetime"
    try:
        info["news"] = url_mail_news
        info["name"] = item.xpath(xpath_item_name)[0].replace('\xa0', ' ')
        url = item.xpath(xpath_item_href)[0]
        info["href"] = str(url)
        r_new = requests.get(url, headers=headers)
        dom_new = html.fromstring(r_new.text)
        data = dom_new.xpath(xpath_data)[0]
        format_data = datetime.strptime(data, "%Y-%m-%dT%H:%M:%S%z")
        info["data"] = str(format_data)
        info_list_mail.append(info)
    except Exception as e:
        print(e)

print(info_list_mail)

with MongoClient(MONGO_URI) as client:
    news_db = client[MONGO_DB]
    mail_ru_db = news_db["mail_ru"]
    add_unique_job(mail_ru_db, info_list_mail)

##lenta
r = requests.get(url_lenta, headers=headers)
main_xpath = "//div[contains(@class, 'js-main__content')]//div[@class = 'item']"
dom = html.fromstring(r.text)
items = dom.xpath(main_xpath)

info_list_lenta = []

for item in items:
    info = {}
    xpath_item_name = "//section//a/text()"
    xpath_item_href = "//section//h2/a/@href"
    xpath_data = "//section//time/@datetime"
    try:
        info["news"] = url_lenta
        info["name"] = item.xpath(xpath_item_name)[0].replace('\xa0', ' ')
        url = item.xpath(xpath_item_href)[0]
        info["href"] = url_lenta + str(url)
        format_data = item.xpath(xpath_data)[0]
        info["data"] = str(format_data)
        info_list_lenta.append(info)
    except Exception as e:
        print(e)

print(info_list_lenta)

with MongoClient(MONGO_URI) as client:
    news_db = client[MONGO_DB]
    lenta_db = news_db["lenta"]
    add_unique_job(lenta_db, info_list_lenta)

## yandex
r = requests.get(url_ya_news, headers=headers)
main_xpath = "//div[@class = 'news-app__content']//div[contains(@class, 'news-top-flexible-stories')]//div[contains(" \
             "@class, 'mg-grid__col')] "
dom = html.fromstring(r.text)
items = dom.xpath(main_xpath)

info_list_ya = []

for item in items:
    info = {}
    xpath_item_name = "//a[@class = 'mg-card__link']//h2/text()"
    xpath_item_href = "//a[@class = 'mg-card__link']/@href"
    xpath_data = "//span[@class = 'mg-card-source__time']/text()"
    try:
        info["news"] = url_ya_news
        info["name"] = item.xpath(xpath_item_name)[0].replace('\xa0', ' ')
        url = item.xpath(xpath_item_href)[0]
        info["href"] = str(url)
        data = item.xpath(xpath_data)[0]
        data2 = f"{datetime.now().day} {datetime.now().month} {datetime.now().year} {data}"
        format_data = datetime.strptime(data2, "%d %m %Y %H:%M")
        info["data"] = str(format_data)
        info_list_ya.append(info)
    except Exception as e:
        print(e)

print(info_list_ya)

with MongoClient(MONGO_URI) as client:
    news_db = client[MONGO_DB]
    ya_news_db = news_db["ya_news"]
    add_unique_job(ya_news_db, info_list_ya)
