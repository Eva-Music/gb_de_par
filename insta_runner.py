from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings

from lesson_8.spiders.instagram import InstaSpider
from lesson_8 import settings

if __name__ == '__main__':

    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    users_list = list(input().split(","))
    # users_list = ['cookingfreak696', 'grangeprodto']
    process = CrawlerProcess(settings=crawler_settings)
    process.crawl(InstaSpider, users_list=users_list)

    process.start()